package com.greenwichnexus.ktm;

//import com.arellomobile.android.push.BasePushMessageReceiver;
//import com.arellomobile.android.push.PushManager;
//import com.arellomobile.android.push.utils.RegisterBroadcastReceiver;
//
//import com.pushwoosh.support.v4.app.NotificationCompat;

import java.io.File;

import info.androidhive.imageslider.FullScreenViewActivity;



import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Toast;

public class Second extends Activity {
	
	File file;
	//push//
		private static final String APP_ID = "00000";//pushwush appid
		private static final String SENDER_ID = "905552771638";//google project ID

		boolean broadcastPush = true;
	//push dogwood-thought-715// 240615850876//
	
	
	
	private boolean isBackPressed = false;
	static final String TAG = "GCMDemo";
	ImageView ktm2014,fb,twit,you,web,call,gallery,memo,contact,about,test,uslinks,livealtrs,memdirtry,mancmty;
	WebView w;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.second);
		
		//push//
      //  registerReceivers();

		//Create and start push manager
//		PushManager pushManager = new PushManager(this, APP_ID, SENDER_ID);
//		pushManager.onStartup(this);
//		PushManager.setMultiNotificationMode(this);
//		checkMessage(getIntent(),true);
		
		//push//
		gallery=(ImageView) findViewById(R.id.gallery);
        gallery.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				ConnectivityManager cn=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		        NetworkInfo nf=cn.getActiveNetworkInfo();
		        if(nf != null && nf.isConnected()==true )
		        {
				
				
				Intent intent=new Intent(Second.this,FullScreenViewActivity.class);
				intent.putExtra("id", "http://cochinfood.com/ktmsociety/admin/action.php?action=sendimages");
				startActivity(intent);
				
			}
			
		        else
		        {  
		        	
		        	
		        	
		            Toast.makeText(getApplicationContext(), "You are offline", Toast.LENGTH_LONG).show();
		            
		            Toast.makeText(getApplicationContext(), "Please check the connection settings", Toast.LENGTH_LONG).show();
		            
		        }
				   
		        
	     }
		});
		call=(ImageView) findViewById(R.id.call);
        call.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent callIntent = new Intent(Intent.ACTION_VIEW);
			    callIntent.setData(Uri.parse("tel:+914842203156"));
			    startActivity(callIntent);	
			}
		});
        web=(ImageView) findViewById(R.id.web);
		web.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), web.class);
				i.putExtra("id", "http://keralatravelmart.org/");
				startActivity(i);
			}
		});
		memo=(ImageView) findViewById(R.id.Memorandum_of_Association);
		memo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				
				Intent i=new Intent(getApplicationContext(), JoinKtmActivity.class);
			//	i.putExtra("id","http://keralatravelmart.org/content/application-membership");
				startActivity(i);
				
				/*Intent i = new Intent(getApplicationContext(), Memo.class);
				startActivity(i);*/
			}
		});
		about=(ImageView) findViewById(R.id.about_ktm);
		about.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), About.class);
				
				startActivity(i);
			}
		});
		
		mancmty=(ImageView) findViewById(R.id.Management_Committee);
		mancmty.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				file = new File(new File(getCacheDir(),"")+"cacheFileMgmt.srl");
				
				if(file.exists()){
				Intent i=new Intent(getApplicationContext(), ManagementActivity.class);
				//i.putExtra("id","http://www.keralatravelmart.org/content/managing-committee-2013-15");
				startActivity(i);
				}
				
				
			}
		});
		
		contact=(ImageView) findViewById(R.id.contact_us);
		contact.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				Intent i=new Intent(getApplicationContext(), webo.class);
				i.putExtra("id","http://keralatravelmart.org/content/contact");
				startActivity(i);
				
				
				/*Intent i=new Intent(getApplicationContext(), you.class);
				i.putExtra("id","file:///android_asset/contact.html");
				startActivity(i);*/

			}
		});
		
		
		memdirtry=(ImageView) findViewById(R.id.Member_Directory);
		memdirtry.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(), webo.class);
				i.putExtra("id","http://keralatravelmart.org/members");
				startActivity(i);
				
			}
		});
		
		ktm2014=(ImageView) findViewById(R.id.ktm_2014);
ktm2014.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				ConnectivityManager cn=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		        NetworkInfo nf=cn.getActiveNetworkInfo();
		        if(nf != null && nf.isConnected()==true )
		        {
				
				
		        	Intent intent=new Intent(Second.this,FullScreenViewActivity.class);
					intent.putExtra("id", "http://cochinfood.com/ktm2014/admin/action.php?action=sendimages");
					startActivity(intent);
				
			}
			
		        else
		        {  
		        	
		        	
		        	
		            Toast.makeText(getApplicationContext(), "You are offline", Toast.LENGTH_LONG).show();
		            
		            Toast.makeText(getApplicationContext(), "Please check the connection settings", Toast.LENGTH_LONG).show();
		            
		        }
				   
		        
	     }
		});
		livealtrs=(ImageView) findViewById(R.id.live_alerts);
		
		livealtrs.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				Intent intent=new Intent(Second.this,PushActivity.class);
			//	intent.putExtra("id", "http://cochinfood.com/ktm2014/admin/action.php?action=sendimages");
				startActivity(intent);
				
				
				
				
				
			}
		});
		
		
		test=(ImageView) findViewById(R.id.test);
		
		test.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(Second.this,Testimonial.class);
				startActivity(intent);
				// TODO Auto-generated method stub
				
			}
		});
		
		uslinks=(ImageView) findViewById(R.id.uslink);
		
		
		uslinks.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				file = new File(new File(getCacheDir(),"")+"cacheFile.srl");
				if(file.exists()){
				Intent intent=new Intent(Second.this,UsefulLinksActivity.class);
					startActivity(intent);
				}
				
			}
		});
		
		
		w = (WebView) findViewById(R.id.webView1);

		w.setWebViewClient(new WebViewClient() {
			@Override
			public void onReceivedError(WebView view, int errorCode,
					String desc, String failing) {
				Log.e(TAG, " Error occured while loading the web page at Url"
						+ failing + "." + desc);
				view.loadUrl("about:blank");
				Toast.makeText(getBaseContext(), "No Internet found",
						Toast.LENGTH_SHORT).show();
				super.onReceivedError(view, errorCode, desc, failing);
			}
		});
		w.loadUrl("http://cochinfood.com/ktm/flashnews/news.php");
		w.setVerticalScrollBarEnabled(false);
		w.setHorizontalScrollBarEnabled(false);
		w.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return true;
			}
		});	
		fb=(ImageView) findViewById(R.id.facebook);
		fb.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					Intent i = new Intent(Intent.ACTION_VIEW, Uri
							.parse("fb://profile/275154209171208"));
					startActivity(i);

				} catch (Exception e) {

					/*
					 * Intent i=new Intent(Intent.ACTION_VIEW,Uri.parse(
					 * "https://www.facebook.com/KeralaTravelMart"));
					 * startActivity(i);
					 */
					Intent i = new Intent(getApplicationContext(), web.class);
					i.putExtra("id",
							"https://www.facebook.com/KeralaTravelMarty");
					startActivity(i);

				}

			}
		});
		twit=(ImageView) findViewById(R.id.twitter);
		twit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?user_id=385304362"));
					startActivity(i);

				} catch (Exception e) {

					/*
					 * Intent i=new Intent(Intent.ACTION_VIEW,Uri.parse(
					 * "https://twitter.com/KTMsociety")); startActivity(i);
					 */
					Intent i = new Intent(getApplicationContext(), web.class);
					i.putExtra("id", "https://twitter.com/KTMsociety");
					startActivity(i);

				}

			}
		});
		you=(ImageView) findViewById(R.id.youtube);
		you.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try {

					Intent intent = null;
					try {
						intent = new Intent(Intent.ACTION_VIEW);
						intent.setPackage("com.google.android.youtube");
						intent.setData(Uri
								.parse("http://www.youtube.com/user/keralatourismdotorg"));
						startActivity(intent);
					} catch (ActivityNotFoundException e) {
						Intent i = new Intent(getApplicationContext(),
								web.class);
						i.putExtra("id",
								"http://www.youtube.com/user/keralatourismdotorg");
						startActivity(i);

						/*
						 * intent = new Intent(Intent.ACTION_VIEW);
						 * intent.setData(Uri.parse(
						 * "http://www.youtube.com/user/keralatourismdotorg"));
						 * startActivity(intent);
						 */
					}

				} catch (Exception e) {

					/*
					 * Intent i=new Intent(Intent.ACTION_VIEW,Uri.parse(
					 * "http://youtube.com/keralatourismdotorg"));
					 * startActivity(i);
					 */

					Intent i = new Intent(getApplicationContext(), web.class);
					i.putExtra("id", "http://youtube.com/keralatourismdotorg");
					startActivity(i);

				}

			}
		});
}
	

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			isBackPressed = true;
			finish();
		}
		return super.onKeyDown(keyCode, event);

	}
//push//
    
    /**
	 * Called when the activity receives a new intent.
	 */
	public void onNewIntent(Intent intent)
	{

		super.onNewIntent(intent);
		//Toast.makeText(getApplicationContext(), "onNewIntent", 0).show();
		//Log.i("onnewintent", "yeah");
		//have to check if we've got new intent as a part of push notification
	//	checkMessage(intent,false);
	}

    
    
@Override
public void onResume()
{
	super.onResume();
	Data.APP_RUNNING=true;
	//Re-register receivers on resume
	//registerReceivers();
}

@Override
public void onPause()
{
	super.onPause();
	Data.APP_RUNNING=false;
	//Unregister receivers on pause
	//unregisterReceivers();
}
@Override
public void onBackPressed() {
	super.onBackPressed();
	finish();
}

	//Push message receiver
//	private BroadcastReceiver mReceiver = new BasePushMessageReceiver()
//	{
//		@Override
//		protected void onMessageReceive(Intent intent)
//		{
//			//JSON_DATA_KEY contains JSON payload of push notification.
//			Toast.makeText(getApplicationContext(), "broadcastReciever:"+intent.getExtras().getString(JSON_DATA_KEY), 0).show();
//			doOnMessageReceive(intent.getExtras().getString(JSON_DATA_KEY),false);
//		}
//	};
//	
//	NotificationCompat.Builder notification;
//	PendingIntent pIntent;
//	NotificationManager manager;
//	Intent resultIntent;
//	TaskStackBuilder stackBuilder;
//	public void doOnMessageReceive(String message,boolean showNotification)
//	{
//		Toast.makeText(getApplicationContext(), "New Notification", 0).show();
//
//		if(showNotification)
//		{
//			Intent intent= new Intent(this,Notification.class);
//			intent.putExtra("json", message);
//			
//			System.out.println(message);
//			
//			Log.i("json", message);
//			startActivity(intent);
//		}
//	}
//
//
//	//Registration receiver
//	BroadcastReceiver mBroadcastReceiver = new RegisterBroadcastReceiver()
//	{
//		@Override
//		public void onRegisterActionReceive(Context context, Intent intent)
//		{
//			checkMessage(intent,false);
//		}
//	};
//
//	private void checkMessage(Intent intent,boolean showNotification)
//	{
//		if (null != intent)
//		{
//			if (intent.hasExtra(PushManager.PUSH_RECEIVE_EVENT))
//			{
//				doOnMessageReceive(intent.getExtras().getString(PushManager.PUSH_RECEIVE_EVENT),showNotification);
//			}
//			/*else if (intent.hasExtra(PushManager.REGISTER_EVENT))
//				{
//					doOnRegistered(intent.getExtras().getString(PushManager.REGISTER_EVENT));
//				}
//				else if (intent.hasExtra(PushManager.UNREGISTER_EVENT))
//				{
//					doOnUnregisteredError(intent.getExtras().getString(PushManager.UNREGISTER_EVENT));
//				}
//				else if (intent.hasExtra(PushManager.REGISTER_ERROR_EVENT))
//				{
//					doOnRegisteredError(intent.getExtras().getString(PushManager.REGISTER_ERROR_EVENT));
//				}
//				else if (intent.hasExtra(PushManager.UNREGISTER_ERROR_EVENT))
//				{
//					doOnUnregistered(intent.getExtras().getString(PushManager.UNREGISTER_ERROR_EVENT));
//				}*/
//
//			resetIntentValues();
//		}
//	}
//
//	private void resetIntentValues()
//	{
//		Intent mainAppIntent = getIntent();
//
//		if (mainAppIntent.hasExtra(PushManager.PUSH_RECEIVE_EVENT))
//		{
//			mainAppIntent.removeExtra(PushManager.PUSH_RECEIVE_EVENT);
//		}
//		else if (mainAppIntent.hasExtra(PushManager.REGISTER_EVENT))
//		{
//			mainAppIntent.removeExtra(PushManager.REGISTER_EVENT);
//		}
//		else if (mainAppIntent.hasExtra(PushManager.UNREGISTER_EVENT))
//		{
//			mainAppIntent.removeExtra(PushManager.UNREGISTER_EVENT);
//		}
//		else if (mainAppIntent.hasExtra(PushManager.REGISTER_ERROR_EVENT))
//		{
//			mainAppIntent.removeExtra(PushManager.REGISTER_ERROR_EVENT);
//		}
//		else if (mainAppIntent.hasExtra(PushManager.UNREGISTER_ERROR_EVENT))
//		{
//			mainAppIntent.removeExtra(PushManager.UNREGISTER_ERROR_EVENT);
//		}
//
//		setIntent(mainAppIntent);
//	}
//	public void registerReceivers()
//	{
//		IntentFilter intentFilter = new IntentFilter(getPackageName() + ".action.PUSH_MESSAGE_RECEIVE");
//
//		if(broadcastPush)
//			registerReceiver(mReceiver, intentFilter);
//
//		registerReceiver(mBroadcastReceiver, new IntentFilter(getPackageName() + "." + PushManager.REGISTER_BROAD_CAST_ACTION));		
//	}
//
//	public void unregisterReceivers()
//	{
//		//Unregister receivers on pause
//		try
//		{
//			unregisterReceiver(mReceiver);
//		}
//		catch (Exception e)
//		{
//			// pass.
//		}
//
//		try
//		{
//			unregisterReceiver(mBroadcastReceiver);
//		}
//		catch (Exception e)
//		{
//			//pass through
//		}
//	}
//    
//    
//    
    

  //push//
}

