package com.greenwichnexus.ktm;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.PushService;

public class ApplicationClass extends Application {

  @Override
  public void onCreate() {
    super.onCreate();

    // Add your initialization code here
    Parse.initialize(this, "3gcVncub8PQbgFSC5h596Xb58q4uiEa6bI56z4Mi", "YT5o7s6udkqcoFF4Au76c8AZFng37nBpoGmqY0Zu");

    PushService.subscribe(getApplicationContext(), "test", MainActivity.class);
	PushService.setDefaultPushCallback(getApplicationContext(), MainActivity.class);
	

    ParseUser.enableAutomaticUser();
    ParseACL defaultACL = new ParseACL();
      
    // If you would like all objects to be private by default, remove this line.
    defaultACL.setPublicReadAccess(true);
    
    ParseACL.setDefaultACL(defaultACL, true);
   
  }
}
