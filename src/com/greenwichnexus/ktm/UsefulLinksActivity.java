package com.greenwichnexus.ktm;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class UsefulLinksActivity  extends Activity{

	private String htmlcontent,heading;
	 ArrayAdapter<JSONObject> tweetAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.usefull_links);
		
		tweetAdapter = new ArrayAdapter<JSONObject>(this, 0) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null)
                    convertView = getLayoutInflater().inflate(R.layout.usefull_link_item, null);

                try {
                TextView handle = (TextView)convertView.findViewById(R.id.useful_links);
             
				handle.setText((position+1)+" . "+this.getItem(position).getString("sitename").toString());
			
             // final ListCompany listcompany=this.getItem(position);
              final JSONObject j=this.getItem(position);
              handle.setOnClickListener(new OnClickListener() {
      			
      			@Override
      			public void onClick(View v) {
      				try {Intent browserIntent;
					
						browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+j.getString("sitelink")));
				
      				startActivity(browserIntent);
      				// TODO Auto-generated method stub
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
      				
      			}
      		});
                }
                catch (Exception e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}

                return convertView;
            }
		};
		
		 ObjectInputStream in;
			try {
				
				
				in = new ObjectInputStream(new FileInputStream(new File(new File(getCacheDir(),"")+"cacheFile.srl")));
			
				String jsonArray=(String) in.readObject();
				
			        in.close();
			    
			        JSONArray jarray=new JSONArray(jsonArray);
			      
			        for( int k=0;k<jarray.length();k++){
			        	
			        	
			        		 
					        JSONObject j=jarray.getJSONObject(k);
					       Log.e("====jhkj===", j.toString());
					        tweetAdapter.add(j);
						  
				        
			        	
			        }
			     
			
			
			

	}
			catch (Exception e){}
			ListView listview=(ListView)findViewById(R.id.list_usefull);
			listview.setAdapter(tweetAdapter);

}
	
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
		Intent i=new Intent(getApplicationContext(),Second.class);
		startActivity(i);
	}
	
	
	}
