package com.greenwichnexus.ktm;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class JoinKtmActivity  extends Activity{
	ServiceHandler sh;
		EditText name;
	 	EditText address;
	EditText mobile;
	EditText email;
		Button reg;
		String response;
		protected ProgressDialog proDialog;
		protected void startLoading() {
		    proDialog = new ProgressDialog(this);
		    proDialog.setMessage("Trying to Register. Please Wait.....");
		    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		    proDialog.setCancelable(false);
		    proDialog.show();
		}

		protected void stopLoading() {
		    proDialog.dismiss();
		    proDialog = null;
		}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.joinktmactivity);
		
			name=(EditText)findViewById(R.id.name_value);
	 address=(EditText)findViewById(R.id.address_value);
	 mobile=(EditText)findViewById(R.id.mobile_value);
 email=(EditText)findViewById(R.id.email_value);
		Button reg=(Button)findViewById(R.id.register_btn);
		reg.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				startLoading();
				new Registerreq().execute();
					
			}
		});
		
		
	}
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
		Intent i=new Intent(getApplicationContext(),Second.class);
		startActivity(i);
	}
	
	 private class Registerreq extends AsyncTask<Void, Void, Void> {
	   	 
	        @Override
	        protected void onPreExecute() {
	            super.onPreExecute();
	     
	        }
	 
	        @Override
	        protected Void doInBackground(Void... arg0) {
	        	 sh = new ServiceHandler();
					
					String url="http://cochinfood.com/ktmpush/api/sendMail.php?name="+name.getText().toString()+"&&addr="+address.getText().toString()+"&&email="+email.getText().toString()+"&&mob="+mobile.getText().toString()+"";
				//	String url1="http://cochinfood.com/ktmpush/api/getmembers.php";
					
					
					response=sh.makeServiceCall(url, ServiceHandler.GET);
					
	            return null;
	        }
	 
	        @Override
	        protected void onPostExecute(Void result) {
	            super.onPostExecute(result);
	            Log.e("====Response=====", response);
	             stopLoading();
	             if(response.equals("success")){
	            	 Toast.makeText(getApplicationContext(), "Your registration request is received", Toast.LENGTH_LONG).show();
	            	finish();
	            	 Intent i=new Intent(getApplicationContext(),Second.class);
		 		      startActivity(i);
	             }
	             else {
	            	 Toast.makeText(getApplicationContext(), "Please fill all required fields", Toast.LENGTH_LONG).show();
				}
	             
	             
//				 AlertDialog.Builder adb = new AlertDialog.Builder(getApplicationContext());
//				 
//				 
//				 		   // adb.setView( );
//				 
//				 adb.setIcon(R.drawable.ic_launcher);
//				 		   
//				 
//				 
//				 		   
//				 		   
//				
//				if(response.equals("success"))
//				{
//					 adb.setTitle("Your now Joined with us");
//			 		   
//					 
//					 
//			 		   
//					 
//					 
//			 		    adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//			 		        public void onClick(DialogInterface dialog, int which) {
//			 		        finish();
//			 		      Intent i=new Intent(getApplicationContext(),Second.class);
//			 		      startActivity(i);
//			 		          
//			 		           
//			 		      } });
//				}
//				else
//				{
//					 adb.setTitle("Some error Occured ...!");
//			 		
//			 		    adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//			 		        public void onClick(DialogInterface dialog, int which) {
//			 
//			 		       
//			 		           
//			 		      } });
//				}
//				
//				
//				 adb.show();
//					 
//				
	         }
	 
	    }
	
	
		
}
