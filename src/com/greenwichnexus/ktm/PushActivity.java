package com.greenwichnexus.ktm;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;



public class PushActivity extends Activity {
	MyDB db;
	JSONObject json;
	ArrayAdapter<JSONObject> tweetAdapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.push_list);
	
		
}
	
	@Override
	public void onResume(){
	    super.onResume();
	    
	    db=new MyDB(getApplicationContext());
		tweetAdapter = new ArrayAdapter<JSONObject>(this, 0) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null)
                    convertView = getLayoutInflater().inflate(R.layout.push_item, null);

                try {
                TextView handle = (TextView)convertView.findViewById(R.id.push_headings);
             
				handle.setText(this.getItem(position).getString("head").toString());
			
             // final ListCompany listcompany=this.getItem(position);
              final JSONObject j=this.getItem(position);
              handle.setOnClickListener(new OnClickListener() {
      			
      			@Override
      			public void onClick(View v) {
      				try {Intent i=new Intent(getApplicationContext(),PushSingleView.class);
      				i.putExtra("head",j.getString("head"));
      				i.putExtra("body",j.getString("body"));
      				//i.putExtra("isoffer", listcompany.isoffer);
      				startActivity(i);
      				// TODO Auto-generated method stub
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
      				
      			}
      		});
              
              TextView handle1 = (TextView)convertView.findViewById(R.id.push_greater);
              
		
			
           // final ListCompany listcompany=this.getItem(position);
            
            handle1.setOnClickListener(new OnClickListener() {
    			
    			@Override
    			public void onClick(View v) {
    				try {Intent i=new Intent(getApplicationContext(),PushSingleView.class);
    				i.putExtra("head",j.getString("head"));
    				i.putExtra("body",j.getString("body"));
    				//i.putExtra("isoffer", listcompany.isoffer);
    				startActivity(i);
    				// TODO Auto-generated method stub
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    				
    			}
    		});
              
                }
                catch (Exception e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}

                return convertView;
            }
		};
		try{
		Cursor c=db.selectRecords();
		
		
		Log.d("dbsize", Integer.toString(db.size()));
		
		for(int i=0;i<db.size();i++){
			
			json=new JSONObject();
			
			c.moveToPosition(i);
			
			json.put("head", c.getString(1));
			json.put("body", c.getString(2));
			
			Log.d("head"+i, c.getString(1));
			Log.d("body"+i, c.getString(2));
			
			//c.moveToPrevious();
			
			tweetAdapter.add(json);

		}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
						  
	ListView listview=(ListView)findViewById(R.id.list_push);
			listview.setAdapter(tweetAdapter);

	    
	    // put your code here...

	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
		Intent i=new Intent(getApplicationContext(),Second.class);
		startActivity(i);
	}

		
	
	
	
	@SuppressLint("NewApi")
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public void createNotification(View view) {
		    // Prepare intent which is triggered if the
		    // notification is selected
		    Intent intent = new Intent(this, PushActivity.class);
		    PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);

		    // Build notification
		    // Actions are just fake
		    Notification noti = new Notification.Builder(this)
		        .setContentTitle("new notification")
		        .setContentText("Subject").setSmallIcon(R.drawable.ic_launcher)
		        .setContentIntent(pIntent)
		       .build();
		    NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		    // hide the notification after its selected
		    noti.flags |= Notification.FLAG_AUTO_CANCEL;

		    notificationManager.notify(0, noti);

		  }


	
	public void showData(View view){
		
	}
}
