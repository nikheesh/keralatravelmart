package com.greenwichnexus.ktm;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ManagementActivity extends Activity{

	private String htmlcontent,heading;
	 ArrayAdapter<JSONObject> tweetAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.commitee_activity);
		
		tweetAdapter = new ArrayAdapter<JSONObject>(this, 0) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null)
                    convertView = getLayoutInflater().inflate(R.layout.commitee_item_body, null);

                try {
                TextView name = (TextView)convertView.findViewById(R.id.mgmt_name);
                TextView desig = (TextView)convertView.findViewById(R.id.mgmt_desig);
                TextView head = (TextView)convertView.findViewById(R.id.mgmt_head);
                
				//name.setText((position+1)+" . "+this.getItem(position).getString("sitename").toString());
			
             // final ListCompany listcompany=this.getItem(position);
              final JSONObject j=this.getItem(position);
              if(j.getBoolean("ishead"))
            	 
              { 
            	 if(j.getString("catid").equals("Member")) 
            	 { head.setText("Committee Members");
            	 
            	 head.setVisibility(View.VISIBLE);  
            	 }
            	 else
            	 { head.setText("Executive");
            	 
                 head.setVisibility(View.GONE);  
            	 }
              }
              else
            	  head.setVisibility(View.GONE);
              name.setText(j.getString("name").toString());
              desig.setText(j.getString("desig").toString());
              
              
             
                }
                catch (Exception e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}

                return convertView;
            }
		};
		
		 ObjectInputStream in;
			try {
				
				
				in = new ObjectInputStream(new FileInputStream(new File(new File(getCacheDir(),"")+"cacheFileMgmt.srl")));
			
				String jsonArray=(String) in.readObject();
				
			        in.close();
			    
			        JSONArray jarray=new JSONArray(jsonArray);
			        String head="Top Management";
			        for( int k=0;k<jarray.length();k++){
			        	 
					        JSONObject j;
					        j=jarray.getJSONObject(k);
					        Log.e("jsons", j.toString());
					       if(!head.equals(j.getString("catid")))
					    	   j.put("ishead", true);
					    	   else
					    		   j.put("ishead", false);
					    	   
					       
					        
					        
					        
					     //  Log.e("====jhkj===", j.toString());
					        tweetAdapter.add(j);
						  head=j.getString("catid");
				        
			        	
			        }
			     
			
			
			

	}
			catch (Exception e){}
			ListView listview=(ListView)findViewById(R.id.list_commitee);
			listview.setAdapter(tweetAdapter);

}
	}
