package com.greenwichnexus.keralatravelmart;

import com.greenwichnexus.keralatravelmart.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Window;
import android.widget.TextView;

public class PushSingleView extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.pushsingleitem);
		
		
		
	}
	
	@Override
	public void onResume(){
	    super.onResume();
	    TextView head=(TextView)findViewById(R.id.heading);
		TextView body=(TextView)findViewById(R.id.body1);
	head.setText( getIntent().getExtras().getString("head").toString());
	body.setText( getIntent().getExtras().getString("body").toString());
	body.setMovementMethod(new ScrollingMovementMethod());
	
	    // put your code here...

	}
	@Override
	protected void onNewIntent(Intent intent) {
	    super.onNewIntent(intent);
	    // getIntent() should always return the most recent
	    setIntent(intent);
	   
	    
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
		Intent i=new Intent(getApplicationContext(),PushActivity.class);
		startActivity(i);
	}
}
