
package com.greenwichnexus.keralatravelmart;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import com.greenwichnexus.keralatravelmart.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;



public class webviewmember extends Activity {

	private final static String HOST = "www.almullatravel.com/";
	private WebView wb;
    String urll;
	TextView tv;
	ProgressBar PB;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
	    
	    
	    Intent i=getIntent();
		 urll=i.getExtras().getString("id");
	    setContentView(R.layout.webview);
	    wb = (WebView) findViewById(R.id.webView1);
	   // wb.setWebViewClient(new WebViewClient());
	   
	    PB=(ProgressBar) findViewById(R.id.progressBar1);
	    
	   // tv.startAnimation(AnimationUtils.loadAnimation(webo.this,android.R.anim.fade_in));
	    //Make the webview invisible
	    
	    ConnectivityManager cn=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf=cn.getActiveNetworkInfo();
        if(nf != null && nf.isConnected()==true )
        {
           
	    wb.setVisibility(View.INVISIBLE);
	    WebSettings webSettings = wb.getSettings();
	    webSettings.setJavaScriptEnabled(true);
	    
	    
	    wb.setWebChromeClient(new WebChromeClient() {
	    	
	    	
	    	
	         
	         
	    	
	       /* public void onPageFinished(WebView view, String url){
	            //Inject javascript code to the url given
	            //Not display the element
	            wb.loadUrl("javascript:(function(){"+"document.getElementById('Id').style.display ='none';"+"})()");
	            //Call to a function defined on my myJavaScriptInterface 
	            wb.loadUrl("javascript: window.CallToAnAndroidFunction.setVisible()");
	        }    
	        */
	         
	        public void onProgressChanged(WebView wb, int newProgress)
          	{
          		super.onProgressChanged(wb, newProgress);
          		PB.setProgress(newProgress);
          		if(newProgress==100)
          		{
          			PB.setVisibility(View.GONE);
          			 wb.loadUrl("javascript:(function(){"+"document.getElementById('Id').style.display ='none';"+"})()");
          			 wb.loadUrl("javascript: window.CallToAnAndroidFunction.setVisible()");
          		}
          		else
          		{
          			PB.setVisibility(View.VISIBLE);
          		}
          	}
        
	        
	        
	         
        
	         
	        
	    });
	    wb.loadUrl(urll);
	    
	    wb.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
            {
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                view.loadUrl(url);
                return true;
            }
        });
	    
	    
	    
	    
        }
        else
        {
        
        	
        	 Toast.makeText(this, "You are offline", Toast.LENGTH_LONG).show();
             
             Toast.makeText(this, "Please check the connection settings", Toast.LENGTH_LONG).show();
             finish();
        }
	    

	    //Add a JavaScriptInterface, so I can make calls from the web to Java methods
	    wb.addJavascriptInterface(new myJavaScriptInterface(), "CallToAnAndroidFunction");
	    ////////////////////////
	    
	    
	   
	    
	    
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if(event.getAction() == KeyEvent.ACTION_DOWN){
	        switch(keyCode)
	        {
	        case KeyEvent.KEYCODE_BACK:
	            if(wb.canGoBack()){
	                wb.goBack();
	            }else{
	                finish();
	            }
	            return true;
	        }

	    }
	    return super.onKeyDown(keyCode, event);
	}


	 public class myJavaScriptInterface {
	     @JavascriptInterface
	     public void setVisible(){
	         runOnUiThread(new Runnable() {

	            @Override
	            public void run() {
	                wb.setVisibility(View.VISIBLE);                 
	            }
	        });
	     }

	 }}
