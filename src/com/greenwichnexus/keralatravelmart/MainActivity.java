package com.greenwichnexus.keralatravelmart;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.greenwichnexus.keralatravelmart.R;



import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

public class MainActivity extends Activity {
	private static int SPLASH_TIME_OUT = 1000;
	private boolean isBackPressed = false;
	 private JSONArray json2,json,jsonArray_Push;
	   String jsonStr_ug;
	   String jsonStr;
	   String jsonStrPG;
	   String jsonArrayPush;
	   MyDB db;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_main);
		 db=new MyDB(getApplicationContext());
		
		
		
		new GetPGData().execute();
		new Handler().postDelayed(new Runnable() {
			public void run() {
				if (!isBackPressed) {
					Intent i = new Intent(MainActivity.this,
							Second.class);
					startActivity(i);
					finish();
				}
			}
		}, SPLASH_TIME_OUT);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			isBackPressed = true;
			finish();
		}
		return super.onKeyDown(keyCode, event);

	}
	
	 private class GetPGData extends AsyncTask<Void, Void, Void> {
	   	 
	        @Override
	        protected void onPreExecute() {
	            super.onPreExecute();
	     
	        }
	 
	        @Override
	        protected Void doInBackground(Void... arg0) {
	        	// Log.e("Inside doinbackgroumd  web ", "3");
	 	        
	        	// Creating service handler class instance
	            ServiceHandler sh = new ServiceHandler();
	            ServiceHandler sh1 = new ServiceHandler();
	            ServiceHandler sh_push = new ServiceHandler();
	            
	            // Making a request to url and getting response
	            String url = "http://cochinfood.com/ktmpush/api/getlinks.php";
	            String url1 ="http://cochinfood.com/ktmpush/api/getmembers.php";
	            String url_push ="http://cochinfood.com/ktmpush/api/getpush.php";
	            
	             jsonStrPG = sh.makeServiceCall(url, ServiceHandler.GET);
	         jsonStr=sh1.makeServiceCall(url1,ServiceHandler.GET );
	         jsonArrayPush=sh_push.makeServiceCall(url_push,ServiceHandler.GET );
	         try{
	            if (jsonStrPG != null) 
	            	{
	               

	                	  json2=new JSONArray(jsonStrPG);
	                                          
	                } 
//	            else {
//	                Log.e("ServiceHandler", "Couldn't get any data from the url for PG Data");
//	            	}
	            
	            if (jsonStr != null) 
            	{
               

                	  json=new JSONArray(jsonStr);
                                          
                }
	            if (jsonArrayPush != null) 
            	{
               

                	  jsonArray_Push=new JSONArray(jsonArrayPush);
                                          
                }
	            
	            
	         }
	            catch (JSONException e) {
                    e.printStackTrace();
                } 
	            return null;
	        }
	 
	        @Override
	        protected void onPostExecute(Void result) {
	            super.onPostExecute(result);
	            Log.e("Inside postexe  web ", "3");
			      
	           // Toast.makeText(getApplicationContext(), "successfully saved ...!",10).show();
	            ObjectOutput out,out2;
				try {
					 if (jsonStrPG != null) {
				          
					out = new ObjectOutputStream(new FileOutputStream(new File(getCacheDir(),"")+"cacheFile.srl"));
					//Log.e("Inside before filewrite ", "3");
					  
					out.writeObject(json2.toString());
					//Log.e("Inside before filewrite ", "3");
					  
	                  out.close();	}
					 
					 
					 if (jsonStr != null) {
				          
							out2 = new ObjectOutputStream(new FileOutputStream(new File(getCacheDir(),"")+"cacheFileMgmt.srl"));
							//Log.e("Inside before filewrite ", "3");
							  
							out2.writeObject(json.toString());
							//Log.e("Inside before filewrite ", "3");
							  
			                  out2.close();	}
				
					 
					 
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					//Crashlytics.logException(e);
				}
				
				try {
					 if (jsonArrayPush != null) {
				         db.deleteAll();
				         for(int i=0;i<jsonArray_Push.length();i++){
				        	 JSONObject json = jsonArray_Push.getJSONObject(i);
				        	 db.createRecords(json.getString("headfield"), json.getString("bodyfield"));
				         }
						 
						 
						 
						 
					 }
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					//Crashlytics.logException(e);
				}
				
           
              
	         }
	 
	    }
	 

}
