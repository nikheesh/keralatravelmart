package com.greenwichnexus.keralatravelmart;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class MyDB{  

private MyDatabaseHelper dbHelper;  

private SQLiteDatabase database;  

public  String NOTI_TABLE="Notification"; // name of table 

public  String DATE="datecreation";
public  String HEAD="head"; // id value for employee
public  String BODY="body";  // name of employee
/** 
 * 
 * @param context 
 */  
public MyDB(Context context){  
    dbHelper = new MyDatabaseHelper(context);  
    database = dbHelper.getWritableDatabase(); 
    Log.d("database object in mydb constructor", database.toString());
}


public long createRecords(String head, String body){  
	Log.e("before date"," ");
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	String date = sdf.format(Calendar.getInstance().getTime());
   ContentValues values = new ContentValues();  
   values.put(DATE, date); 
   
   values.put(HEAD, head);  
   values.put(BODY, body);
   long l = database.insert(NOTI_TABLE, null, values);
   Log.d("insert values", "head :"+head+"/body :"+body);
   Log.d("insert result", Long.toString(l));
   return l;
}    

public Cursor selectRecords() {
	
	
	
   String[] cols = new String[] {DATE,HEAD,BODY };  
   Cursor mCursor = database.query(true, NOTI_TABLE,cols,null  
            , null, null, null, "Datetime("+DATE+") DESC", null);  
   if (mCursor != null) {  
     mCursor.moveToFirst();  
   }  
   return mCursor; 
   // iterate to get each value.
}



public Cursor selectAll() {
	   
	String[] cols = new String[] {DATE,HEAD,BODY };  
	   Cursor mCursor = database.query(true, NOTI_TABLE,cols,null  
	            , null, null, null, "Datetime("+DATE+") DESC", null);  
	   if (mCursor != null) {  
	     mCursor.moveToFirst();  
	   }  
	   return mCursor; 
	   // iterate to get each value.
	}



public int size(){
return (int) DatabaseUtils.queryNumEntries(database, "Notification");
}

public void deleterow()
{ 
	Log.d("delete row got executed", "delete");
	database.execSQL("DELETE FROM "+NOTI_TABLE+"  WHERE "+DATE+" = (SELECT "+DATE+" FROM "+NOTI_TABLE+" ORDER BY Datetime("+DATE+") LIMIT 1);");
}

public void deleteAll()
{ database.delete(NOTI_TABLE, null, null);
	//database.execSQL("DELETE FROM "+NOTI_TABLE+"  WHERE "+DATE+" = (SELECT "+DATE+" FROM "+NOTI_TABLE+" ORDER BY Datetime("+DATE+") LIMIT 1);");
}
public void droptable(){
	Log.d("drop got executed", "drop");
	database.execSQL("DROP TABLE "+NOTI_TABLE);

}

public void close(){
	Log.d("database", "db closed");
	database.close();
}

public void createtable(){
	Log.d("create got executed", "create");
	database.execSQL("CREATE TABLE "+NOTI_TABLE+"("+DATE+","+HEAD+","+BODY+");");

}
}