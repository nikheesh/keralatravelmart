package com.greenwichnexus.keralatravelmart;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.greenwichnexus.keralatravelmart.webo.myJavaScriptInterface;
import com.greenwichnexus.keralatravelmart.R;

public class UsefullLinkWeb extends Activity {

//	private final static String HOST = "www.almullatravel.com/";
	private WebView wb;
    String urll;
	//TextView tv;
//	ProgressBar PB;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
	    
	    
	    Intent i=getIntent();
		 urll=i.getExtras().getString("id");
	    setContentView(R.layout.usefullinkweb);
	    wb = (WebView) findViewById(R.id.web_usefullinks);
	  
	    
	    ConnectivityManager cn=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf=cn.getActiveNetworkInfo();
        if(nf != null && nf.isConnected()==true )
        {
           
	    
	    
	  
	    wb.loadUrl(urll);
	    
	    wb.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
            {
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                view.loadUrl(url);
                return true;
            }
        });
	    
	    
	    
	    
        }
        else
        {
        
        	
        	 Toast.makeText(this, "You are offline", Toast.LENGTH_LONG).show();
             
             Toast.makeText(this, "Please check the connection settings", Toast.LENGTH_LONG).show();
             finish();
        }
	    
 
	    
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if(event.getAction() == KeyEvent.ACTION_DOWN){
	        switch(keyCode)
	        {
	        case KeyEvent.KEYCODE_BACK:
	            if(wb.canGoBack()){
	                wb.goBack();
	            }else{
	                finish();
	            }
	            return true;
	        }

	    }
	    return super.onKeyDown(keyCode, event);
	}

	
	 }

