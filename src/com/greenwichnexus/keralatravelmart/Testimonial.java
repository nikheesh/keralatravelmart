package com.greenwichnexus.keralatravelmart;

import com.greenwichnexus.keralatravelmart.R;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.view.Window;
import android.widget.TextView;

public class Testimonial extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.testimonial);
		TextView txt1=(TextView)findViewById(R.id.test_text1);
		TextView txt2=(TextView)findViewById(R.id.test_text2);
		TextView txt3=(TextView)findViewById(R.id.test_text3);
		String msg1="		\"I am delighted to say I exhibited at a big travel show last weekend and have 1 in London next weekend and the interest in Kerala is very high. I used some of the new experiences I enjoyed on my trip to KTM to share with guests who all were excited.So there has already been a positive impact from my attendance at KTM\". �<b> Lynn Stewart � Escape to India, UK</b>";


String msg2="		\"We really enjoyed the KTM this year. It was easy to get around and the variety and presentations were excellent. Many thanks to all who organized it. I hope to do more business with you in future\". -<b> Anita Larkin- Ladies on Tour, UK</b>";

String msg3="		\"As in the previous (2012) year, the KTM 2014 program was a sell out and one of the top notch and professionally organized travel fairs we had the opportunity to attend.   We have returned but energized to continue and solicit Kerala as a truly magical destination in our markets\". � <b>Poulos Sime Yeshaneh � Air Promotion Group, Sweden</b>";
		
		
		
		txt1.setText(Html.fromHtml(msg1));
		txt2.setText(Html.fromHtml(msg2));
		txt3.setText(Html.fromHtml(msg3));
	}

}
