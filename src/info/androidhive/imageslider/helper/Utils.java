package info.androidhive.imageslider.helper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Point;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

public class Utils {

	private Context _context;
	public ArrayList<String> fileAddress = new ArrayList<String>();
	int count =0;

	// constructor
	public Utils(Context context) {
		this._context = context;
         
	}
	
	// constructor
	public Utils(Context context, ArrayList<String> tFileAddress, int tCount) {
		this._context = context;
         fileAddress=tFileAddress;
         count=tCount;
	}

	/*
	 * Reading file paths from Server
	 */
	public ArrayList<String> getFileAddress() {
		
		// check for directory
		if (AppConstant.SERVER_URL!= null) {

			// Check for count
			if ((fileAddress.isEmpty()) || (count==0)) {
				//Toast.makeText(_context,AppConstant.SERVER_URL+ " is empty. Please load some images in it !",Toast.LENGTH_LONG).show();
			} else {
				return fileAddress;
			}

		} else {
			AlertDialog.Builder alert = new AlertDialog.Builder(_context);
			alert.setTitle("Error!");
			alert.setMessage(AppConstant.SERVER_URL
					+ " is not valid! Please set the AppConstant.java class");
			alert.setPositiveButton("OK", null);
			alert.show();
		}

		return fileAddress;
	}

	/*
	 * Check supported file extensions
	 * 
	 * @returns boolean
	 */
	private boolean IsSupportedFile(String filePath) {
		String ext = filePath.substring((filePath.lastIndexOf(".") + 1),
				filePath.length());

		if (AppConstant.FILE_EXTN
				.contains(ext.toLowerCase(Locale.getDefault())))
			return true;
		else
			return false;

	}

	/*
	 * getting screen width
	 */
	@SuppressLint("NewApi")
	public int getScreenWidth() {
		int columnWidth;
		WindowManager wm = (WindowManager) _context
				.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();

		final Point point = new Point();
		try {
			display.getSize(point);
		} catch (java.lang.NoSuchMethodError ignore) { // Older device
			point.x = display.getWidth();
			point.y = display.getHeight();
		}
		columnWidth = point.x;
		return columnWidth;
	}
	

}
